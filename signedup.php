<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/index.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <title>MovieFlix Thank-You</title>
</head>
<body>
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="index.html" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery.html">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="container-fluid">
        <div class="thanks text-center">
            <img class="img-fluid" src="images/thankyou.gif" alt="Thank You" width="720" height="381">
        </div>
    </div>

    <!-- Footer-->
    <div class="container-fluid">
        <footer class="footer">
            
                <div class="row">
                    <div class="col-md-3">
                        <span class="footcol">Contact</span><br>
                        <span class="addr">
                                10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria. <br>
                                Email: movieflix@movieflix.com <br>
                                Phone no: 090-234-567-8910  <br>
                                website: wwww.movieflix.ng  <br>
                        </span>  
                    </div>
                    <div class="col-md-3">
                        <span class="footcol">Genres</span><br>
                        <ul class="list-unstyled">
                            <a href="#" class="lin"><li>Drama</li></a>
                            <a href="#" class="lin"><li>Action</li></a>
                            <a href="#" class="lin"><li>Thriller</li></a>
                            <a href="#" class="lin"><li>Adventure</li></a>
                            <a href="#" class="lin"><li>Romance</li></a>
                            <a href="#" class="lin"><li>Horror</li></a>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <span class="footcol">Up Coming</span><br>
                        <ul class="list-unstyled">
                                <a href="#" class="lin"><li>First Flight</li></a>
                                <a href="#" class="lin"><li>King Kunta</li></a>
                                <a href="#" class="lin"><li>Brilla Man's life</li></a>
                                <a href="#" class="lin"><li>Story of Charles</li></a>
                                <a href="#" class="lin"><li>No Woman no cry</li></a>
                                <a href="#" class="lin"><li>Marry me Lucy</li></a>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <span class="footcol">Sponsors</span><br>
                        <p class="footlogo"><img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
                    </div>
                </div>
            
        </footer>
    </div>

    

    






    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>