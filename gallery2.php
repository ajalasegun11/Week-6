<!DOCTYPE php>
<php lang="en">
<?php
    session_start();
    #print_r($_SESSION);
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/home.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Gallery</title>
</head>
<body>
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="second.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe2.php">Subscribe</a>
                    </li>
                    <?php
                        if(isset($_SESSION['id'])){
                            echo '
                            <li class="nav-item">
                            <form action="home.php" method="post">
                                <button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                        		<a class="nav-link" href="profile.php">Profile</a>
                    		</li>
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    ?>
                </ul>
            </div>
        </nav>       
    </div>

    <section class="gallery-block grid-gallery">
        <div class="container">
            <div class="heading">
                <h2 class="bla">Our movie collections</h2>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/1.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/1.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/2.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/2.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/3.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/3.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/4.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/4.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/5.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/5.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/6.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/6.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/7.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/7.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/8.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/8.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/9.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/9.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/10.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/10.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/11.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/11.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/12.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/12.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/13.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/13.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/14.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/14.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/15.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/15.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/16.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/16.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/17.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/17.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/18.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/18.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/19.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/19.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/20.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/20.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <a class="lightbox" href="img/new/21.jpg">
                        <img class="img-fluid image scale-on-hover" src="img/new/21.jpg">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer-->
    <div class="container-fluid">
    <footer class="footer">
        
            <div class="row">
                <div class="col-md-3">
                    <span class="footcol">Contact</span><br>
                    <span class="addr">
                            10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria. <br>
                            Email: movieflix@movieflix.com <br>
                            Phone no: 090-234-567-8910  <br>
                            website: wwww.movieflix.ng  <br>
                    </span>  
                </div>
                <div class="col-md-3">
                    <span class="footcol">Genres</span><br>
                    <ul class="list-unstyled">
                        <a href="#" class="lin"><li>Drama</li></a>
                        <a href="#" class="lin"><li>Action</li></a>
                        <a href="#" class="lin"><li>Thriller</li></a>
                        <a href="#" class="lin"><li>Adventure</li></a>
                        <a href="#" class="lin"><li>Romance</li></a>
                        <a href="#" class="lin"><li>Horror</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Up Coming</span><br>
                    <ul class="list-unstyled">
                            <a href="#" class="lin"><li>First Flight</li></a>
                            <a href="#" class="lin"><li>King Kunta</li></a>
                            <a href="#" class="lin"><li>Brilla Man's life</li></a>
                            <a href="#" class="lin"><li>Story of Charles</li></a>
                            <a href="#" class="lin"><li>No Woman no cry</li></a>
                            <a href="#" class="lin"><li>Marry me Lucy</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Sponsors</span><br>
                    <p class="footlogo"><img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
                </div>
            </div>
        
    </footer>
    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script>
            baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
</body>
</php>