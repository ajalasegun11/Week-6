<?php 
    session_start();
	#print_r($_SESSION);
	if(!$_SESSION['id']){
		header("Location: home.php");
	}
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/home.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Rental</title>
</head>
<body>
    <div class="contents">

    
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="second.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe2.php">Subscribe</a>
                    </li>
                    <?php
                        if(isset($_SESSION['id'])){
							echo '
							<li class="nav-item">
                        		<a class="nav-link" href="profile.php">Profile</a>
                    		</li>
                            <li class="nav-item">
                            <form action="home.php" method="post" id="logoutForm">
                                <button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    ?>
                </ul>
            </div>
        </nav>

<!-- Process form -->
<?php
#connect to database!
include "includes/dbConnect.php";
if($conn){
	#echo "Yes there is a connection";
}else{
	#echo "Noooo Connection here!";
}

	$file = $first = $last = $bio = "";
	$fileErr = $firstErr = $lastErr = $bioErr = "";


	#if update is clicked;
	if(isset($_POST['update'])){

		function clean($input){
            $input = trim($input);
            $input = stripcslashes($input);
            $input = htmlspecialchars($input);
            return $input;
		}
		if($_FILES['file']['error'] == 4){
			$fileErr = "Please upload a picture";
			
		}else{
			print_r($_FILES);
			$fileType = $_FILES['file']['type'];
			if($fileType=="image/jpg"||$fileType=="image/jpeg"||$fileType=="image/png"){
				$filename = $_FILES['file']['name'];
            	$fileTmpName = $_FILES['file']['tmp_name'];
            	$fileError = $_FILES['file']['error'];
				$fileDir = "uploads1/";
				$filePath = $fileDir.$filename;
				#echo $filePath;
				if($_FILES['file']['error'] == 1){
					$fileErr = "Ensure that your image isn't more than 2MB";
				}
			}else{
				$fileErr = "Only image files allowed";
			}
			
		}

		if(empty($_POST['first'])){
			$firstErr = "First name is empty";
		}else{
			$first = clean($_POST['first']);
			#echo $first;
		}

		if(empty($_POST['last'])){
            $lastErr = "Please enter your lastname";
        }else{
            $last = clean($_POST['last']);
            #echo $last; 
		}
		
		if(empty($_POST['bio'])){
            $bioErr = "Please enter your bio";
        }else{
            $bio = clean($_POST['bio']);
            #echo $bio;
		}
		
		#if there are no errors
		if($fileErr == "" && $firstErr == "" && $lastErr == "" && $bioErr == ""){
			$message = "";
			#move uploaded file to the directory
			if(!file_exists("$fileDir")){
				mkdir($fileDir, 0777);
			}
			chmod($fileDir, 0777);
			
			$fh = fopen($filePath,"a+");
			if(copy($fileTmpName, $filePath)){
				echo "File uploaded to directory. Something went wrong";
			fclose($fh);
			
				#clean for db safety
				$first = mysqli_real_escape_string($conn, $first);
				$last = mysqli_real_escape_string($conn, $last);
				$bio = mysqli_real_escape_string($conn, $bio);
				$filePath = mysqli_real_escape_string($conn, $filePath);

				#update database of the user 
				if(isset($_SESSION['id'])){
					$id = $_SESSION['id'];
					$sql = "UPDATE users SET firstname='$first', lastname='$last', bio='$bio', picture='$filePath' WHERE id='$id'";
					$result = mysqli_query($conn, $sql);
					if($result){
						$message = "data updated";
						header("Location: profile.php");
					}else{
						$message = "data not updated";
					}

				}
			}else{
				echo "failed to move file";
			}
		}
	}

	 
?>


			<!-- Main body -->
			<div class="container">
					<div class="row profile">   
					<?php echo $message ?>                 
                    <div class="col-md-12 prof">                        
                        <form method="post" enctype="multipart/form-data" action="update-profile.php">
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Upload your profile picture</label>
                                <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
								<span><?php echo $fileErr ?></span>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">First Name</label>
                                <input type="text" name="first" class="form-control" id="formGroupExampleInput" placeholder="Enter your first name">
								<span><?php echo $firstErr ?></span>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Last Name</label>
                                <input type="text" name="last" class="form-control" id="formGroupExampleInput2" placeholder="Enter your last name">
								<span><?php echo $lastErr ?></span>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Bio</label>
                                <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3"placeholder="About yourself"></textarea>
								<span><?php echo $bioErr ?></span>
                            </div>
                            <button name="update" type="submit" class="btn btn-primary">Submit</button>
                        </form>                        
                    </div>
                </div>
			</div>





			<!-- Footer-->
			<div class="container-fluid">
				<footer class="footer">
					<div class="row">
						<div class="col-md-3">
							<span class="footcol">Contact</span>
							<br>
							<span class="addr">
								10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria.
								<br> Email: movieflix@movieflix.com
								<br> Phone no: 090-234-567-8910
								<br> website: wwww.movieflix.ng
								<br>
							</span>
						</div>
						<div class="col-md-3">
							<span class="footcol">Genres</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>Drama</li>
								</a>
								<a href="#" class="lin">
									<li>Action</li>
								</a>
								<a href="#" class="lin">
									<li>Thriller</li>
								</a>
								<a href="#" class="lin">
									<li>Adventure</li>
								</a>
								<a href="#" class="lin">
									<li>Romance</li>
								</a>
								<a href="#" class="lin">
									<li>Horror</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Up Coming</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>First Flight</li>
								</a>
								<a href="#" class="lin">
									<li>King Kunta</li>
								</a>
								<a href="#" class="lin">
									<li>Brilla Man's life</li>
								</a>
								<a href="#" class="lin">
									<li>Story of Charles</li>
								</a>
								<a href="#" class="lin">
									<li>No Woman no cry</li>
								</a>
								<a href="#" class="lin">
									<li>Marry me Lucy</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Sponsors</span>
							<br>
							<p class="footlogo">
								<img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
						</div>
					</div>
				</footer>
			</div>

			<script src="js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
			<script>
				baguetteBox.run('.grid-gallery', {
					animation: 'slideIn'
				});
			</script>
	</body>
</php>