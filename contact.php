<!DOCTYPE php>
<php lang="en">
<?php
    session_start();
    #print_r($_SESSION);
?>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/home.js"></script>
		<script src="js/jquery.validate.js"></script>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/home.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
		<title>MovieFlix Sign-up</title>
	</head>

	<body>
		<!-- Nav, Slide and center word-->
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
				<a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
				 aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="home.php">Home
								<span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="sign-up.php">Sign-up</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="gallery2.php">Gallery</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="contact.php">Contact Us</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="subscribe.php">Subscribe</a>
						</li>
						<?php
                        if(isset($_SESSION['id'])){
                            echo '
                            <li class="nav-item">
                            <form action="home.php" method="post">
								<button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    	?>
					</ul>
				</div>
			</nav>
		</div>
		<!-- Form-->
		<?php
			$name = $email = $message = "";
			$nameError = $emailErr = $messageErr = "";
			$emailSuccess = "";
			$emailError = "";

			function cleanInput($input){
				$input = trim($input);
				$input = stripslashes($input);
				$input = htmlspecialchars($input);
				return $input;
			}

			if($_SERVER["REQUEST_METHOD"] == "POST"){
				if(empty($_POST['name'])){
					$nameErr = "Please enter your name";
				}else{
					$name = cleanInput($_POST['name']);
					if(!preg_match("/^[a-zA-Z ]*$/", $name)){
						$nameErr = "Enter only letters and whitespaces";
					}
				}

				if(empty($_POST['email'])){
					$emailErr = "Please enter your email address";
				}else{
					$email = cleanInput($_POST['email']);
					if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
						$emailErr = "Invalid email address";
					}
				}

				if(empty($_POST['message'])){
					$messageErr = "Please enter your message";
				}else{
					$message = cleanInput($_POST['message']);
				}

				if($nameErr == "" && $emailErr == "" && $messageErr == ""){
					$name = cleanInput($_POST['name']);
					$email = cleanInput($_POST['email']);
					$message = cleanInput($_POST['message']);

					#create csv file to save all messages
					$file = fopen("messages.csv", 'a');
					$data = array($name,$email,$message);
					fputcsv($file,$data);
					fclose($file);

					#create message csv file for email replys
					$file2 = fopen('message.csv', 'w');
					$data2 = array($name,$email,$message);
					fputcsv($file2,$data2);
					fclose($file2);

					#retreive data to send email 
					$file3 = fopen('message.csv', 'r');
					$data3 = fgetcsv($file3);
					$to = $data3[1];
					$subject = "MovieFlix has received your message";
					$body = "Hi $name,
					
A message from $to has been received. We will get back to you soonest.
					
					
Warm regards,
MovieFlix Group";
					$headers = "From: MovieFlixGroup \r\n";
					if(mail($to,$subject,$body,$headers)){
						$emailSuccess = "Thank you $name, a confirmation mail has be sent to $to.";				
					}else{
						$emailError = "Sorry $name, something went wrong. Please try again.";
					}

				}else{
					$name = $email = $message = "";
				}
			}
		?>
		<div class="container hold">
			<div class="page-header sub_head">
				<h1>Send us a message</h1>
				<p><?php echo "$emailError $emailSuccess" ?></p>	
			</div>
			<div class="cholder">
				<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" id="contact">
					<div class="form-group row">
						<label for="inputName3" class="col-sm-2 col-form-label">Name</label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" id="inputName3" placeholder="Enter your name">
							<small id="nameHelp" class="form-text text-muted">
								<?php echo $nameErr ?>
							</small>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
						<div class="col-sm-10">
							<input type="text" name="email" class="form-control" id="inputEmail3" placeholder="Enter your email">
							<small id="emailHelp" class="form-text text-muted">
								<?php echo $emailErr ?>
							</small>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputTextarea3" class="col-sm-2 col-form-label">Message</label>
						<div class="col-sm-10">
							<textarea name="message" class="form-control" id="inputTextarea3" rows="3" placeholder="Enter your message"></textarea>
							<small id="messageHelp" class="form-text text-muted">
								<?php echo $messageErr ?>
							</small>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-10">
							<button type="submit" class="btn btn-primary">Send Message</button>
						</div>
					</div>
				</form>
			</div>
		</div>


		<!-- Footer-->
		<div class="container-fluid">
			<footer class="footer">

				<div class="row">
					<div class="col-md-3">
						<span class="footcol">Contact</span>
						<br>
						<span class="addr">
							10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria.
							<br> Email: movieflix@movieflix.com
							<br> Phone no: 090-234-567-8910
							<br> website: wwww.movieflix.ng
							<br>
						</span>
					</div>
					<div class="col-md-3">
						<span class="footcol">Genres</span>
						<br>
						<ul class="list-unstyled">
							<a href="#" class="lin">
								<li>Drama</li>
							</a>
							<a href="#" class="lin">
								<li>Action</li>
							</a>
							<a href="#" class="lin">
								<li>Thriller</li>
							</a>
							<a href="#" class="lin">
								<li>Adventure</li>
							</a>
							<a href="#" class="lin">
								<li>Romance</li>
							</a>
							<a href="#" class="lin">
								<li>Horror</li>
							</a>
						</ul>
					</div>
					<div class="col-md-3">
						<span class="footcol">Up Coming</span>
						<br>
						<ul class="list-unstyled">
							<a href="#" class="lin">
								<li>First Flight</li>
							</a>
							<a href="#" class="lin">
								<li>King Kunta</li>
							</a>
							<a href="#" class="lin">
								<li>Brilla Man's life</li>
							</a>
							<a href="#" class="lin">
								<li>Story of Charles</li>
							</a>
							<a href="#" class="lin">
								<li>No Woman no cry</li>
							</a>
							<a href="#" class="lin">
								<li>Marry me Lucy</li>
							</a>
						</ul>
					</div>
					<div class="col-md-3">
						<span class="footcol">Sponsors</span>
						<br>
						<p class="footlogo">
							<img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
					</div>
				</div>

			</footer>
		</div>
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
		<script>
			baguetteBox.run('.grid-gallery', {
				animation: 'slideIn'
			});
		</script>
	</body>
</php>