<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/home.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Rental</title>
</head>
<body>
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe.php">Subscribe</a>
                    </li>
                </ul>
            </div>
        </nav>

    <div>
    <?php
        // Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers. backup seperated with ;
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'peterwale25@gmail.com';                 // SMTP username
    $mail->Password = 'ajalabistababe';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('peterwale25@gmail.com', 'Segun Ajala');
    $mail->addAddress('ajalasegun11@gmail.com', '');     // Add a recipient
    //$mail->addAddress('');               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
    ?>
    </div>
        

        
        
    
    <!-- Footer-->
    <div class="container-fluid">
    <footer class="footer">
        
            <div class="row">
                <div class="col-md-3">
                    <span class="footcol">Contact</span><br>
                    <span class="addr">
                            10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria. <br>
                            Email: movieflix@movieflix.com <br>
                            Phone no: 090-234-567-8910  <br>
                            website: wwww.movieflix.ng  <br>
                    </span>  
                </div>
                <div class="col-md-3">
                    <span class="footcol">Genres</span><br>
                    <ul class="list-unstyled">
                        <a href="#" class="lin"><li>Drama</li></a>
                        <a href="#" class="lin"><li>Action</li></a>
                        <a href="#" class="lin"><li>Thriller</li></a>
                        <a href="#" class="lin"><li>Adventure</li></a>
                        <a href="#" class="lin"><li>Romance</li></a>
                        <a href="#" class="lin"><li>Horror</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Up Coming</span><br>
                    <ul class="list-unstyled">
                            <a href="#" class="lin"><li>First Flight</li></a>
                            <a href="#" class="lin"><li>King Kunta</li></a>
                            <a href="#" class="lin"><li>Brilla Man's life</li></a>
                            <a href="#" class="lin"><li>Story of Charles</li></a>
                            <a href="#" class="lin"><li>No Woman no cry</li></a>
                            <a href="#" class="lin"><li>Marry me Lucy</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Sponsors</span><br>
                    <p class="footlogo"><img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
                </div>
            </div>
    </footer>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script>
            baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
</body>
</php>