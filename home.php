<?php 
    session_start();
    #print_r($_SESSION);
    include "includes/logout.inc.php";
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/home.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Rental</title>
</head>
<body>
    <div class="contents">

    
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="second.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe2.php">Subscribe</a>
                    </li>
                    <?php
                        if(isset($_SESSION['id'])){
                            echo '
                            <li class="nav-item">
                        		<a class="nav-link" href="profile.php">Profile</a>
                    		</li>
                            <li class="nav-item">
                            <form action="home.php" method="post" id="logoutForm">
                                <button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    ?>
                </ul>
            </div>
        </nav>

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/1.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/2.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/3.jpg" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/4.jpg" alt="fourth slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/5.jpg" alt="fifth slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/6.jpg" alt="sixth slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/7.jpg" alt="seventh slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/8.jpg" alt="eight slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/9.jpg" alt="ninth slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/10.jpg" alt="tenth slide">
                </div>
            </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
        </div>

        <div class="cont text-center">
        <p>
            <?php 
                if(isset($_SESSION['id'])){
                    echo "<p id='wel-font'>Welcome, ".$_SESSION['user']."</p>";    
                };
            ?>
            For 15 years, MovieFlix has been providing our esteemed customers with the latest blockbuster movies.
            We pride ourselves at delivering new movies for rental as early as one week after official release in the cinema.
        </p>
        </div>        
    </div>
    <!-- New jQuery features-->
    <div class="container-fluid but">
        <div class="row togl">
            <div class="col-md-4 col-sm-4"><button type="button" class="btn-lg btn-outline-primary">Testimonials</button></div>
            <div class="col-md-4 col-sm-4"><button type="button" class="btn-lg btn-outline-success">Gallery</button></div>
            <div class="col-md-4 col-sm-4"><button type="button" class="btn-lg btn-outline-info">Sign-up</button></div>
        </div>
        <!-- Testimonials-->
        <div class="row testi">
            <div class="col-md-4 col-sm-12 test ">
                <p>"I found out about MovieFlix Rentals from my friend and I must say that it has been a beautiful experience. They are 
                always up to date with their movies."</p>
                <span>
                    - Titilade
                </span>
            </div>
            <div class="col-md-4 col-sm-12 test ">
                <p>"I got the latest animation relase of 'The Incredibles2' from movieflix. Thank you so much!"</p>
                <span>
                    - Abudu
                </span>
            </div>
            <div class="col-md-4 col-sm-12 test ">
                <p>"Thank you movieflix for alway being there for me when I need a movie to unwind with my family."</p>
                 <span>
                    - Chioma
                 </span>
            </div>
        </div>
        <!-- Gallary2 Preview-->
        <div class="row">
            <section class="gallery-block grid-gallery newgal">
                <div class="container-fluid">
                    <div class="heading">
                       <a href="gallery2.php" class="gal-link"><h2 class="bla">Our Collections</h2></a>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4 item">
                            <a class="lightbox" href="img/new/1.jpg">
                                <img class="img-fluid image scale-on-hover" src="img/new/1.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 item">
                            <a class="lightbox" href="img/new/13.jpg">
                                <img class="img-fluid image scale-on-hover" src="img/new/13.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 item">
                            <a class="lightbox" href="img/new/5.jpg">
                                <img class="img-fluid image scale-on-hover" src="img/new/5.jpg">
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- Form-->
        <div class="row">
            <div class="signup-form hide">
                <form action="sign-up.php" method="post" id="signup">
                    <h2>Sign Up</h2>
                    <p>It's free and only takes a minute.</p>
                    <?php 
                        echo $success;
                        echo $failure;
                        echo $acctExists;
                    ?>
                    <hr>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username" >
                        <span><?php echo $userErr ?></span>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email Address" >
                        <span><?php echo $emailErr ?></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" id="password1">
                        <span><?php echo $pwdErr ?></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" >
                        <span><?php echo $pwd2Err ?></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg" name="sign-up">Sign Up</button>
                    </div>
                    <p class="small text-center">By clicking the Sign Up button, you agree to our <br><a href="#">Terms &amp; Conditions</a>, and <a href="#">Privacy Policy</a>.</p>
                </form>
                <div class="text-center">Already have an account? <a href="#" id="loginlink"><span >Login here</span></a></div>
            </div>
        </div>
    </div>
    
    <!-- Footer-->
    <div class="container-fluid">
    <footer class="footer">
        
            <div class="row">
                <div class="col-md-3">
                    <span class="footcol">Contact</span><br>
                    <span class="addr">
                            10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria. <br>
                            Email: movieflix@movieflix.com <br>
                            Phone no: 090-234-567-8910  <br>
                            website: wwww.movieflix.ng  <br>
                    </span>  
                </div>
                <div class="col-md-3">
                    <span class="footcol">Genres</span><br>
                    <ul class="list-unstyled">
                        <a href="#" class="lin"><li>Drama</li></a>
                        <a href="#" class="lin"><li>Action</li></a>
                        <a href="#" class="lin"><li>Thriller</li></a>
                        <a href="#" class="lin"><li>Adventure</li></a>
                        <a href="#" class="lin"><li>Romance</li></a>
                        <a href="#" class="lin"><li>Horror</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Up Coming</span><br>
                    <ul class="list-unstyled">
                            <a href="#" class="lin"><li>First Flight</li></a>
                            <a href="#" class="lin"><li>King Kunta</li></a>
                            <a href="#" class="lin"><li>Brilla Man's life</li></a>
                            <a href="#" class="lin"><li>Story of Charles</li></a>
                            <a href="#" class="lin"><li>No Woman no cry</li></a>
                            <a href="#" class="lin"><li>Marry me Lucy</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Sponsors</span><br>
                    <p class="footlogo"><img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
                </div>
            </div>
        
        </footer>
    </div>
    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script>
            baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
</body>
</php>