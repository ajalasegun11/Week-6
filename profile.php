<?php 
    session_start();
    #print_r($_SESSION);
	include "includes/logout.inc.php";
	if(!$_SESSION['id']){
		header("Location: home.php");
	}
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/home.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Rental</title>
</head>
<body>
    <div class="contents">

    
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="second.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe2.php">Subscribe</a>
                    </li>
                    <?php
                        if(isset($_SESSION['id'])){
							echo '
							<li class="nav-item">
                        		<a class="nav-link" href="profile.php">Profile</a>
                    		</li>
                            <li class="nav-item">
                            <form action="home.php" method="post" id="logoutForm">
                                <button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    ?>
                </ul>
            </div>
        </nav>

<div class="container">
<h3 class="headalign">Profile </h3><hr>
  <div class="row">
      <div class="col-12">
	  <b>NOTE:</b> Heroku often deletes uploaded folders. Don't be alarmed should you not see your image/video src not displaying. Inspect the page to see the link to the files<br>  
 <div class="card card-inverse card-primary mb-3 text-center">
 			<?php if(isset($_SESSION['id'])) : ?>
				<?php 
					#$conn = mysqli_connect("localhost","db4free_username","username","db4free.net");
					include "includes/dbConnect.php";
					if($conn){
						#echo "Yes there is a connection";
					}else{
						echo "Noooo Connection here!";
					}
					$id = $_SESSION['id'];
					$sql = "SELECT * FROM users WHERE id=$id";
					$result = mysqli_query($conn, $sql);
					if(mysqli_num_rows($result) > 0){
						while($row = mysqli_fetch_assoc($result)){
							$first = $row['firstname'];
							$last = $row['lastname'];
							$bio = $row['bio'];
							$imagePath = $row['picture'];
						}
					}
				?>
            <div class="card-block">
			  <div class="row">
			  	<div class="col-md-6 col-sm-12 text-center">
					<img src="<?php echo $imagePath ?>" alt="Profile Picture" class="btn-md" width="300" height="300">
				</div>
				
				<div class="col-md-6 col-sm-12">
					<h2 class="card-title">Name: <?php echo $first. " ". $last ?> </h2>
					<p class="card-text"><strong>Username: </strong> <?php echo $_SESSION['user']?> </p>
					<p class="card-text"><strong>Email: </strong> <?php echo $_SESSION['email']?> </p>
				    <p class="card-text"><strong>Bio: </strong> <?php echo $bio ?> </p>
					
				</div>
				<?php endif ; ?>         
				<div class="col-md-6 col-sm-12 text-center">
					<a href="update-profile.php"><button class="btn btn-secondary btn-block btn-md"><span class="fa fa-twitter-square"></span> Edit Profile </button></a>
				</div>
			  </div>
              </div>
          </div>
    </div>
  </div> 

	<div class="row">
		<div class="col-md-6"><h3 class="headalign">Videos </h3></div>
		<div class="col-md-6"><a href="upload-video.php"><button class="btn btn-primary float-right">Upload Video</button></a></div>
	
			<?php
				$sql = "SELECT * FROM movies WHERE userID='$id'";
				$result = mysqli_query($conn, $sql);
				if(mysqli_num_rows($result) > 0){
					while($row = mysqli_fetch_assoc($result)){
						$movieID = $row['id'];
						$title = $row['title'];
						$genre = $row['genre'];
						$filePath = $row['video'];
						$rating = $row['rating'];
						echo "
						
						<div class=\"vid-div col-md-6\">
							<video  height=\"300\" width=\"100%\" controls>
								<source src = \"$filePath\" type=\"video/mp4\">
							</video>
							<div class=\"vid-hold\">
								<h4><b>$title</b></h4> 
								<p>$genre</p>

								<div class='rating-container'>
									<div class='rating-stars'>
										<img src='star-images/blank-star.png' id='1_star' movieID=$movieID >
										<img src='star-images/blank-star.png' id='2_star' movieID=$movieID >
										<img src='star-images/blank-star.png' id='3_star' movieID=$movieID >
										<img src='star-images/blank-star.png' id='4_star' movieID=$movieID >
										<img src='star-images/blank-star.png' id='5_star' movieID=$movieID >
										<div id='response'>
											Rating: $rating/5
										</div>
									</div><br>
								</div><br><br>

								<a href=\"update-video.php?movieID=$movieID\"><button type=\"button\" class=\"btn btn-info btn-sm\">Edit</button></a>
								<a href=\"delete-video.php?movieID=$movieID\"><button type=\"button\" class=\"btn btn-danger btn-sm\">Delete</button></a>
							</div>
						</div>
						
						";
						
					}
				}
			?>

			
			<!--<div class="vid-div">
				<video  height="300" width="100%" controls>
					<source src = "<?php echo $filePath ?>" type="video/mp4">
				</video>
				<div class="vid-hold">
					<h4><b><?php echo $title ?></b></h4> 
    				<p><?php echo $genre ?></p>
				</div>
			</div> -->
		
		
	</div>
</div>

			<!-- Footer-->
			<div class="container-fluid">
				<footer class="footer">
					<div class="row">
						<div class="col-md-3">
							<span class="footcol">Contact</span>
							<br>
							<span class="addr">
								10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria.
								<br> Email: movieflix@movieflix.com
								<br> Phone no: 090-234-567-8910
								<br> website: wwww.movieflix.ng
								<br>
							</span>
						</div>
						<div class="col-md-3">
							<span class="footcol">Genres</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>Drama</li>
								</a>
								<a href="#" class="lin">
									<li>Action</li>
								</a>
								<a href="#" class="lin">
									<li>Thriller</li>
								</a>
								<a href="#" class="lin">
									<li>Adventure</li>
								</a>
								<a href="#" class="lin">
									<li>Romance</li>
								</a>
								<a href="#" class="lin">
									<li>Horror</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Up Coming</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>First Flight</li>
								</a>
								<a href="#" class="lin">
									<li>King Kunta</li>
								</a>
								<a href="#" class="lin">
									<li>Brilla Man's life</li>
								</a>
								<a href="#" class="lin">
									<li>Story of Charles</li>
								</a>
								<a href="#" class="lin">
									<li>No Woman no cry</li>
								</a>
								<a href="#" class="lin">
									<li>Marry me Lucy</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Sponsors</span>
							<br>
							<p class="footlogo">
								<img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
						</div>
					</div>
				</footer>
			</div>

			<script src="js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
			<script>
				baguetteBox.run('.grid-gallery', {
					animation: 'slideIn'
				});
			</script>
	</body>
</php>