<!DOCTYPE php>
<php lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.j"></script>
		<script src="js/home.js"></script>
		<script src="js/jquery.validate.js"></script>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/home.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
		<title>MovieFlix Rental</title>
	</head>

	<body>
		<!-- Nav, Slide and center word-->
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
				<a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
				 aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="home.php">Home
								<span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="sign-up.php">Sign-up</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="gallery2.php">Gallery</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="contact.php">Contact Us</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="subscribe.php">Subscribe</a>
						</li>
					</ul>
				</div>
			</nav>


			<div class="container">
            <!-- form handling serverside -->
            <?php


				function cleanInput($input){
					$input = trim($input);
					$input = stripslashes($input);
					$input = htmlspecialchars($input);
					return $input;
				}

				$email = "";
				$emailErr = "";
				$emailSuccess = "";
				$emailError = "";

				if($_SERVER["REQUEST_METHOD"] == "POST"){
					if(empty($_POST['email'])){
						$emailErr = "Please enter an email address";
					}else{
						$email = cleanInput($_POST['email']);
						if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
							$emailErr = "Invalid email address";
						}
					}
					if($emailErr == ""){
						$email = cleanInput($_POST['email']);
						#compile email to a csv file for later use
						$file = fopen('subscribers.csv', 'a');
						$data = array($email);
						fputcsv($file, $data);
						fclose($file);

						#save email address to send confirmation mail
						$file2 = fopen('sendEmailS.csv', 'w');
						$data2 = array($email);
						fputcsv($file2, $data2);
						fclose($file2);

						#get email from file to send confirmatory email
						$file3 = fopen('sendEmailS.csv', 'r');
						$data3 = fgetcsv($file3);					
						$to = $data3[0];
						$subject = "You have successfully subscribed to our newsletter";
						$content = "Hi,
Your email address: $to has been successfully registered to receive our newsletters.

Warm regards,
MovieFlix Team
						";
						$headers = "From: MovieFlix";
						if(mail($to,$subject,$content,$headers)){
							$emailSuccess = "has been registered. Please check your email for confirmation.";
						}else{
							$emailError = "Sorry, something went wrong. Try again.";
						}
						
	
						fclose($file3);
					}else{
						$email = "";	
					}
					
				}
            ?>



				<div class="page-header sub_head">
					<h1>Subscribe to our newsletter</h1>
					<p><?php 
					echo "$email $emailSuccess $emailError" ?></p>
				</div>
				<div class="sholder">
					<form id="subscribe" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
						<div class="form-group row">
							<div class="col-sm-10">
								<input type="text" name="email" class="form-control" id="inputEmail3" placeholder="Enter your email">
								<small id="emailHelp" class="form-text text-muted">
									<?php echo $emailErr ?>
								</small>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-10">
								<button type="submit" class="btn btn-primary">Subscribe</button>
							</div>
						</div>
					</form>
				</div>
			</div>





			<!-- Footer-->
			<div class="container-fluid">
				<footer class="footer">
					<div class="row">
						<div class="col-md-3">
							<span class="footcol">Contact</span>
							<br>
							<span class="addr">
								10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria.
								<br> Email: movieflix@movieflix.com
								<br> Phone no: 090-234-567-8910
								<br> website: wwww.movieflix.ng
								<br>
							</span>
						</div>
						<div class="col-md-3">
							<span class="footcol">Genres</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>Drama</li>
								</a>
								<a href="#" class="lin">
									<li>Action</li>
								</a>
								<a href="#" class="lin">
									<li>Thriller</li>
								</a>
								<a href="#" class="lin">
									<li>Adventure</li>
								</a>
								<a href="#" class="lin">
									<li>Romance</li>
								</a>
								<a href="#" class="lin">
									<li>Horror</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Up Coming</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>First Flight</li>
								</a>
								<a href="#" class="lin">
									<li>King Kunta</li>
								</a>
								<a href="#" class="lin">
									<li>Brilla Man's life</li>
								</a>
								<a href="#" class="lin">
									<li>Story of Charles</li>
								</a>
								<a href="#" class="lin">
									<li>No Woman no cry</li>
								</a>
								<a href="#" class="lin">
									<li>Marry me Lucy</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Sponsors</span>
							<br>
							<p class="footlogo">
								<img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
						</div>
					</div>
				</footer>
			</div>

			<script src="js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
			<script>
				baguetteBox.run('.grid-gallery', {
					animation: 'slideIn'
				});
			</script>
	</body>
</php>