<!DOCTYPE php>
<php lang="en">
<?php
    session_start();
    include "includes/login.inc.php";
    #print_r($_SESSION);
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/home.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Login</title>
</head>
<body>
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="second.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe2.php">Subscribe</a>
                    </li>
                    <?php
                        if(isset($_SESSION['id'])){
                            echo '
                            <li class="nav-item">
                        		<a class="nav-link" href="profile.php">Profile</a>
                    		</li>
                            <li class="nav-item">
                            <form action="home.php" method="post" id="logoutForm">
                                <button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    ?>
                    
                </ul>
            </div>
        </nav>
    </div>   
        <!-- Form-->
        <div class="row">
            <div class="signup-form">
                <form action="login.php" method="post" id="signup">
                    <h2>Login</h2>
                    <p>Login to your account.</p>
                    <?php 
                        echo $success;
                        echo $failure;
                    ?>
                    <hr>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username" >
                        <span><?php echo $userErr ?></span>
                    </div>            
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" id="password1">
                        <span><?php echo $pwdErr ?></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg" name="login">Login</button>
                    </div>
                </form>
            </div>
        </div>    
    <!-- Footer-->
    <div class="container-fluid">
    <footer class="footer">
        
            <div class="row">
                <div class="col-md-3">
                    <span class="footcol">Contact</span><br>
                    <span class="addr">
                            10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria. <br>
                            Email: movieflix@movieflix.com <br>
                            Phone no: 090-234-567-8910  <br>
                            website: wwww.movieflix.ng  <br>
                    </span>  
                </div>
                <div class="col-md-3">
                    <span class="footcol">Genres</span><br>
                    <ul class="list-unstyled">
                        <a href="#" class="lin"><li>Drama</li></a>
                        <a href="#" class="lin"><li>Action</li></a>
                        <a href="#" class="lin"><li>Thriller</li></a>
                        <a href="#" class="lin"><li>Adventure</li></a>
                        <a href="#" class="lin"><li>Romance</li></a>
                        <a href="#" class="lin"><li>Horror</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Up Coming</span><br>
                    <ul class="list-unstyled">
                            <a href="#" class="lin"><li>First Flight</li></a>
                            <a href="#" class="lin"><li>King Kunta</li></a>
                            <a href="#" class="lin"><li>Brilla Man's life</li></a>
                            <a href="#" class="lin"><li>Story of Charles</li></a>
                            <a href="#" class="lin"><li>No Woman no cry</li></a>
                            <a href="#" class="lin"><li>Marry me Lucy</li></a>
                    </ul>
                </div>
                <div class="col-md-3">
                    <span class="footcol">Sponsors</span><br>
                    <p class="footlogo"><img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
                </div>
            </div>
        
    </footer>
    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script>
            baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
</body>
</php>