$(function () {
    $(".btn-outline-primary").click(function () {
        $(".test").slideToggle(1000);
    });

    $(".btn-outline-success").click(function () {
        $(".newgal").slideToggle(1000);
    });

    $(".btn-outline-info").click(function () {
        $(".signup-form").slideToggle(1000);
    });

    $.validator.addMethod("alphanumeral", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
    }, "Letters and numbers only");

    $("#signup").validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: "required",
            confirm_password: {
                required: true,
                equalTo: "#password1",
            },
            username: {
                required: true,
                alphanumeral: true,
            }
        },
        messages: {
            email: {
                required: "Please enter an email address",
                email: "Please enter a <em>valid</em> email address",
            }
        }
    });

    $.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
    }, "Letters only please");

    $("#contact").validate({
        rules: {
            name: {
                required: true,
                lettersonly: true,
            },

            email: {
                required: true,
                email: true,
            },

            message: {
                required: true,
                message: true,
            }
        },

        messages: {
            name: {
                required: "Please enter your name",
                lettersonly: "Letters and Whitespaces only",
            },

            email: {
                required: "Please enter your email",
                email: "Ivalid email address",
            },

            message: {
                required: "Please enter your message",
            },
        }
    });

    $("#subscribe").validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
        },

        messages: {
            email: {
                required: "Please enter your email",
                email: "Ivalid email address",
            },
        },
    });

    //STARS FUNCTIONS
    //CLICKED VARIABLE
    var click_val = 0;
    //Hover over stars
    $('#1_star').hover(function(){
        $('#1_star').attr('src', 'star-images/gold-star.png');
        $('#2_star').attr('src', 'star-images/blank-star.png');
        $('#3_star').attr('src', 'star-images/blank-star.png');
        $('#4_star').attr('src', 'star-images/blank-star.png');
        $('#5_star').attr('src', 'star-images/blank-star.png');
    });
    //On click of 1st star
    $('#1_star').click(function(){
        movieID= $('#1_star').attr('movieID');

        click_val = 1;
        //send rating to database
        $.ajax({
            type: "POST",
            cache: "false",
            url: 'rating-response.php',
            data: {movieID: movieID, click_val: click_val},
            success: function(reply){
                $('#response').html(reply);
            }
        })
    })

    $('#2_star').hover(function(){
        $('#1_star').attr('src', 'star-images/gold-star.png');
        $('#2_star').attr('src', 'star-images/gold-star.png');
        $('#3_star').attr('src', 'star-images/blank-star.png');
        $('#4_star').attr('src', 'star-images/blank-star.png');
        $('#5_star').attr('src', 'star-images/blank-star.png');
    });
    //On click of 1st star
    $('#2_star').click(function(){
        movieID= $('#2_star').attr('movieID');

        click_val = 2;
        //send rating to database
        $.ajax({
            type: "POST",
            cache: "false",
            url: 'rating-response.php',
            data: {movieID: movieID, click_val: click_val},
            success: function(reply){
                $('#response').html(reply);
            }
        })
    })

    $('#3_star').hover(function(){
        $('#1_star').attr('src', 'star-images/gold-star.png');
        $('#2_star').attr('src', 'star-images/gold-star.png');
        $('#3_star').attr('src', 'star-images/gold-star.png');
        $('#4_star').attr('src', 'star-images/blank-star.png');
        $('#5_star').attr('src', 'star-images/blank-star.png');
    });
    //On click of 1st star
    $('#3_star').click(function(){
        movieID= $('#3_star').attr('movieID');

        click_val = 3;
        //send rating to database
        $.ajax({
            type: "POST",
            cache: "false",
            url: 'rating-response.php',
            data: {movieID: movieID, click_val: click_val},
            success: function(reply){
                $('#response').html(reply);
            }
        })
    })

    $('#4_star').hover(function(){
        $('#1_star').attr('src', 'star-images/gold-star.png');
        $('#2_star').attr('src', 'star-images/gold-star.png');
        $('#3_star').attr('src', 'star-images/gold-star.png');
        $('#4_star').attr('src', 'star-images/gold-star.png');
        $('#5_star').attr('src', 'star-images/blank-star.png');
    });
    //On click of 1st star
    $('#4_star').click(function(){
        movieID= $('#4_star').attr('movieID');

        click_val = 4;
        //send rating to database
        $.ajax({
            type: "POST",
            cache: "false",
            url: 'rating-response.php',
            data: {movieID: movieID, click_val: click_val},
            success: function(reply){
                $('#response').html(reply);
            }
        })
    })

    $('#5_star').hover(function(){
        $('#1_star').attr('src', 'star-images/gold-star.png');
        $('#2_star').attr('src', 'star-images/gold-star.png');
        $('#3_star').attr('src', 'star-images/gold-star.png');
        $('#4_star').attr('src', 'star-images/gold-star.png');
        $('#5_star').attr('src', 'star-images/gold-star.png');
    });
    //On click of 1st star
    $('#5_star').click(function(){
        movieID= $('#5_star').attr('movieID');

        click_val = 5;
        //send rating to database
        $.ajax({
            type: "POST",
            cache: "false",
            url: 'rating-response.php',
            data: {movieID: movieID, click_val: click_val},
            success: function(reply){
                $('#response').html(reply);
            }
        })
    
    })

    //turn stars to blank when mouse out
    $('.rating-stars').mouseout(function(){
        if(click_val === 0 || click_val > 5){
            $('#1_star').attr('src', 'star-images/blank-star.png');
            $('#2_star').attr('src', 'star-images/blank-star.png');
            $('#3_star').attr('src', 'star-images/blank-star.png');
            $('#4_star').attr('src', 'star-images/blank-star.png');
            $('#5_star').attr('src', 'star-images/blank-star.png');
        }else if(click_val === 1){
            $('#1_star').attr('src', 'star-images/gold-star.png');
            $('#2_star').attr('src', 'star-images/blank-star.png');
            $('#3_star').attr('src', 'star-images/blank-star.png');
            $('#4_star').attr('src', 'star-images/blank-star.png');
            $('#5_star').attr('src', 'star-images/blank-star.png');
        }else if(click_val === 2){
            $('#1_star').attr('src', 'star-images/gold-star.png');
            $('#2_star').attr('src', 'star-images/gold-star.png');
            $('#3_star').attr('src', 'star-images/blank-star.png');
            $('#4_star').attr('src', 'star-images/blank-star.png');
            $('#5_star').attr('src', 'star-images/blank-star.png');
        }else if(click_val === 3){
            $('#1_star').attr('src', 'star-images/gold-star.png');
            $('#2_star').attr('src', 'star-images/gold-star.png');
            $('#3_star').attr('src', 'star-images/gold-star.png');
            $('#4_star').attr('src', 'star-images/blank-star.png');
            $('#5_star').attr('src', 'star-images/blank-star.png');
        }else if(click_val === 4){
            $('#1_star').attr('src', 'star-images/gold-star.png');
            $('#2_star').attr('src', 'star-images/gold-star.png');
            $('#3_star').attr('src', 'star-images/gold-star.png');
            $('#4_star').attr('src', 'star-images/gold-star.png');
            $('#5_star').attr('src', 'star-images/blank-star.png');
        }else if(click_val === 5){
            $('#1_star').attr('src', 'star-images/gold-star.png');
            $('#2_star').attr('src', 'star-images/gold-star.png');
            $('#3_star').attr('src', 'star-images/gold-star.png');
            $('#4_star').attr('src', 'star-images/gold-star.png');
            $('#5_star').attr('src', 'star-images/gold-star.png');
        }
    });

    //ON CLICKING ON THE SUBMIT BUTTON
    $('#submit-rating').click(function(){
        if(click_val ===0 ||click_val > 5){
            $('#response').html('Please select a star to rate the movie');
        }else{
            $('#response').html('Rating: '+click_val+'/5');
        }
    })

})