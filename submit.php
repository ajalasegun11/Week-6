<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    //Load Composer's autoloader
    require 'vendor/autoload.php';



    $name = $email = $message = "";
    $nameError = $emailErr = $messageErr = "";
    $emailSuccess = "";
    $emailError = "";

    function cleanInput($input){
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(empty($_POST['name'])){
            $nameErr = "Please enter your name";
        }else{
            $name = cleanInput($_POST['name']);
            if(!preg_match("/^[a-zA-Z ]*$/", $name)){
                $nameErr = "Enter only letters and whitespaces";
            }
        }

        if(empty($_POST['email'])){
            $emailErr = "Please enter your email address";
        }else{
            $email = cleanInput($_POST['email']);
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $emailErr = "Invalid email address";
            }
        }

        if(empty($_POST['message'])){
            $messageErr = "Please enter your message";
        }else{
            $message = cleanInput($_POST['message']);
        }

        if($nameErr == "" && $emailErr == "" && $messageErr == ""){
            $name = cleanInput($_POST['name']);
            $email = cleanInput($_POST['email']);
            $message = cleanInput($_POST['message']);

            #create csv file to save all messages
            $file = fopen("messages.csv", 'a');
            $data = array($name,$email,$message);
            fputcsv($file,$data);
            fclose($file);

            #create message csv file for email replys
            $file2 = fopen('message.csv', 'w');
            $data2 = array($name,$email,$message);
            fputcsv($file2,$data2);
            fclose($file2);

            #retreive data to send email 
            $file3 = fopen('message.csv', 'r');
            $data3 = fgetcsv($file3);
            $to = $data3[1];
            
            #send email with phpmailer
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers. backup seperated with ;
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'peterwale25@gmail.com';                 // SMTP username
    $mail->Password = 'ajalabistababe';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('peterwale25@gmail.com', 'Segun Ajala');
    $mail->addAddress($to, '');     // Add a recipient
    //$mail->addAddress('');               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Message received by MovieFlix';
    $mail->Body    = 'Hi '.$name.',<br><br> Your message has been received. We will get back to you soonest.<br><br>
    Warm regards,<br>MovieFlix Group';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo $name.', your message has been received. We will get back to you soonest. Check your email for confirmation';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
        fclose($file3);
            
        }else{
            $name = $email = $message = "";
        }
    }


