<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leap Year Count</title>
</head>
<body>
    <h2>Years between 1980 - 2018 and the Leap Years:</h2>
    <?php
        $firstYear = strtotime("January 1, 1980");
        $firstYear = date("Y", $firstYear);
        $lastYear = strtotime("January 1, 2018");
        $lastYear = date("Y", $lastYear);
        $count = 0;

        for($s=$firstYear; $s<=$lastYear; $s++){
            if(date("L", strtotime("January 1 $s")) == 1){
                echo "$s Leap Year <br>";
                $count++;
            }else{
                echo "$s <br>";
            }
        }
        echo "<br>";
        echo "There are <b><em>$count</em></b> Leap Years between the year 1980 - 2018";
    ?>
</body>
</html>