<?php 
    session_start();
	#print_r($_SESSION);
	if(!$_SESSION['id']){
		header("Location: home.php");
	}
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/home.js"></script>
    <script src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <title>MovieFlix Rental</title>
</head>
<body>
    <div class="contents">

    
    <!-- Nav, Slide and center word-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navig">
            <a class="navbar-brand" href="home.php" id="logo">MovieFlix</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Sign-up</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery2.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="second.php">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="subscribe2.php">Subscribe</a>
                    </li>
                    <?php
                        if(isset($_SESSION['id'])){
							echo '
							<li class="nav-item">
                        		<a class="nav-link" href="profile.php">Profile</a>
                    		</li>
                            <li class="nav-item">
                            <form action="home.php" method="post" id="logoutForm">
                                <button type="submit" class="btn btn-warning" name="logout">Logout</button>
                            </form>
                            </li>
                            ';
                        }else{
                            echo '
                            <li class="nav-item">
                                <a href="login.php"><button type="button" class="btn btn-info">Login</button></a>
                            </li>
                            ';
                        }
                    ?>
                </ul>
            </div>
        </nav>

<!-- Process form -->
<?php
#connect to database!
include "includes/dbConnect.php";
if($conn){
	#echo "Yes there is a connection";
}else{
	#echo "Noooo Connection here!";
}

	$file = $fid = $title = $genre = "";
	$fileErr = $fidErr = $titleErr = $genreErr = "";
	
	#if update is clicked;
	if(isset($_POST['addvideo'])){

		function clean($input){
            $input = trim($input);
            $input = stripcslashes($input);
            $input = htmlspecialchars($input);
            return $input;
		}
	
		if($_FILES['video']['error'] == 4){
			$fileErr = "Please upload a video";
		}else{
			$fileType = $_FILES['video']['type'];
			#print_r($_FILES);
			if($fileType=="video/mp4"||$fileType=="video/ogg"||$fileType=="video/webm"||$fileType=="video/3gp"){
				$filename = $_FILES['video']['name'];
            	$fileTmpName = $_FILES['video']['tmp_name'];
            	$fileError = $_FILES['video']['error'];
				$fileDir = "uploads2/";
				$filePath = $fileDir.$filename;
				
			}else{
				$fileErr = "Ensure that you upload a video file and it shouldn't be more than 2MB";
			}
			
		}

		if(empty($_POST['fid'])){
			header("Location: login.php");
		}else{
			$fid = clean($_POST['fid']);
			
		}

		if(empty($_POST['title'])){
            $titleErr = "Please enter your movie title";
        }else{
            $title = clean($_POST['title']);
          
		}
		
		if(empty($_POST['genre'])){
            $genreErr = "Please enter your bio";
        }else{
            $genre = clean($_POST['genre']);
            
		}
		
		#if there are no errors
		if($fileErr == "" && $fidErr == "" && $titleErr == "" && $genreErr == ""){
			$message = "";
			if(!file_exists("$fileDir")){
				mkdir($fileDir, 0777);
			}
			chmod($fileDir, 0777);
			#move uploaded file to the directory

			$fh = fopen($filePath,"a+");
			if(copy($fileTmpName, $filePath)){
				$message = "File uploaded to directory";
			fclose($fh);
				#clean for db safety
				$fid = mysqli_real_escape_string($conn, $fid);
				$title = mysqli_real_escape_string($conn, $title);
				$genre = mysqli_real_escape_string($conn, $genre);
				$filePath = mysqli_real_escape_string($conn, $filePath);

				#update database of the user using the foreign id which is the same as the user id $fid
					$sql = "INSERT INTO movies (userID, title, genre, video) VALUES('$fid', '$title', '$genre', '$filePath')";
					
					$result = mysqli_query($conn, $sql);
					if($result){
						$message = "data updated";
						header("Location: profile.php");
					}else{
						$message = "data not updated";
					}

				}
			
		}
	}

	 
?>

			<div class="container">
				<?php echo $message ?>
				<div class="row profile">
                    <div class="col-md-12 prof"> 
						<b>NOTE:</b> Please make sure the video you are uploading is not more than 2MB. This is due to Heroku's restriction.                       
                        <form action="upload-video.php" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<?php $fid = $_SESSION['id'] ?>
                                <input id="prodId" name="fid" type="hidden" value="<?php echo $fid ?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Upload your Movie</label>
								<input type="file" name="video" class="form-control-file" id="exampleFormControlFile1">
								<span><?php echo $fileErr ?></span>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Title</label>
								<input type="text" name="title" class="form-control" id="formGroupExampleInput" placeholder="Enter movie title">
								<span><?php echo $titleErr ?></span>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Genre</label>
								<input type="text" name="genre" class="form-control" id="formGroupExampleInput2" placeholder="Enter movie genre">
								<span><?php echo $genreErr ?></span>
                            </div>
                            <button type="submit" name="addvideo" class="btn btn-primary">Submit</button>
                        </form>                        
                    </div>
                </div>
			</div>





			<!-- Footer-->
			<div class="container-fluid">
				<footer class="footer">
					<div class="row">
						<div class="col-md-3">
							<span class="footcol">Contact</span>
							<br>
							<span class="addr">
								10, MM Way, Blasian Drive, off Maiduguri Street, Lokoja, Kogi State, Nigeria.
								<br> Email: movieflix@movieflix.com
								<br> Phone no: 090-234-567-8910
								<br> website: wwww.movieflix.ng
								<br>
							</span>
						</div>
						<div class="col-md-3">
							<span class="footcol">Genres</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>Drama</li>
								</a>
								<a href="#" class="lin">
									<li>Action</li>
								</a>
								<a href="#" class="lin">
									<li>Thriller</li>
								</a>
								<a href="#" class="lin">
									<li>Adventure</li>
								</a>
								<a href="#" class="lin">
									<li>Romance</li>
								</a>
								<a href="#" class="lin">
									<li>Horror</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Up Coming</span>
							<br>
							<ul class="list-unstyled">
								<a href="#" class="lin">
									<li>First Flight</li>
								</a>
								<a href="#" class="lin">
									<li>King Kunta</li>
								</a>
								<a href="#" class="lin">
									<li>Brilla Man's life</li>
								</a>
								<a href="#" class="lin">
									<li>Story of Charles</li>
								</a>
								<a href="#" class="lin">
									<li>No Woman no cry</li>
								</a>
								<a href="#" class="lin">
									<li>Marry me Lucy</li>
								</a>
							</ul>
						</div>
						<div class="col-md-3">
							<span class="footcol">Sponsors</span>
							<br>
							<p class="footlogo">
								<img src="images/footlogo.png" alt="" width="50px" height="50px"> BLAST VENTRUES</p>
						</div>
					</div>
				</footer>
			</div>

			<script src="js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
			<script>
				baguetteBox.run('.grid-gallery', {
					animation: 'slideIn'
				});
			</script>
	</body>
</php>