<?php
    class Videos{
        //DB STUFFS
        private $conn;
        private $table = "movies";
        
        //users properties
        public $id;
        public $userID;
        public $title;
        public $video;
        public $rating;

        //CONSTRUCTOR WITH DB
        public function __construct($db){
            $this->conn = $db;
        }

        //Get ALL videos
        public function getAllVideos(){
            $sql = "SELECT * FROM $this->table";

            //PREPARE STATEMENT
            $stmt = $this->conn->prepare($sql);
            //EXECUTE STATEMENT querry
            $stmt->execute();
            return $stmt;
        }

        //GET ONE video
        public function getOneVideo(){
            $sql = "SELECT * FROM $this->table WHERE id=:id";

            //PREPARE STATEMENT
            $stmt = $this->conn->prepare($sql);

            //BIND PARAMETERS
            //$stmt->bindParam(':id', $id);
            //EXECUTE STATEMENT querry
            $stmt->execute([':id'=> $this->id]);
            
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            //SET PROPERTIES
            $this->id = $row['id'];
            $this->userID = $row['userID'];
            $this->title = $row['title'];
            $this->genre = $row['genre'];
            $this->video = $row['video'];
            $this->rating = $row['rating'];            
        }

        //UPLOAD VIDEO
        public function uploadVideo(){
            $sql = "INSERT INTO $this->table(userID, title, genre, video) VALUES(:userID, :title, :genre, :video)";
            //prepare statement
            $stmt = $this->conn->prepare($sql);
            //clean inputs
            $this->userID = htmlspecialchars(strip_tags($this->userID));
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->genre = htmlspecialchars(strip_tags($this->genre));
            $this->video = htmlspecialchars(strip_tags($this->video));

            $stmt->bindParam(":userID", $this->userID);
            $stmt->bindParam(":title", $this->title);
            $stmt->bindParam(":genre", $this->genre);
            $stmt->bindParam(":video", $this->video);

            //EXECUTE
            if($stmt->execute()){
                return true;
            }

            print_f("Error: %s.\n", $stmt->error);
            return false;
        }

        //UPDATE VIDEO
        public function updateVideo(){
            $sql = "UPDATE $this->table SET title=:title, genre=:genre where id=:id";
            //prepare statement
            $stmt = $this->conn->prepare($sql);
            //clean input
            $this->id = htmlspecialchars(strip_tags($this->id));
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->genre = htmlspecialchars(strip_tags($this->genre));

            //bindParams
            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":title", $this->title);
            $stmt->bindParam(":genre", $this->genre);
            
            //execute
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        }

        //RATE VIDEO
        public function rateVideo(){
            $sql = "UPDATE $this->table SET rating=:rating WHERE id=:id";
            //prepare statement
            $stmt = $this->conn->prepare($sql);
            //clean input
            $this->id = htmlspecialchars(strip_tags($this->id));
            $this->rating = htmlspecialchars(strip_tags($this->rating));
            //bindParams
            //bindParams
            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":rating", $this->rating);

            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        }

        //DELETE VIDEO
        public function deleteVideo(){
            $sql = "DELETE FROM $this->table WHERE id=:id";
            //prepare statement
            $stmt = $this->conn->prepare($sql);
            //clean input
            $this->id = htmlspecialchars(strip_tags($this->id));
            
            //bindParams
            $stmt->bindParam(":id", $this->id);
            
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>