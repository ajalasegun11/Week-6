<?php
    class Users{
        //DB STUFFS
        private $conn;
        private $table = "users";
        
        //users properties
        public $id;
        public $username;
        public $email;
        public $password;
        public $firstname;
        public $lastname;
        public $bio;
        public $picture;

        //CONSTRUCTOR WITH DB
        public function __construct($db){
            $this->conn = $db;
        }

        //Get ALL USERS
        public function getAllUsers(){
            $sql = "SELECT * FROM $this->table";

            //PREPARE STATEMENT
            $stmt = $this->conn->prepare($sql);
            //EXECUTE STATEMENT querry
            $stmt->execute();
            return $stmt;
        }

        //GET ONE USESR
        public function getOneUser(){
            $sql = "SELECT * FROM $this->table WHERE id=:id";

            //PREPARE STATEMENT
            $stmt = $this->conn->prepare($sql);

            //BIND PARAMETERS
            //$stmt->bindParam(':id', $id);
            //EXECUTE STATEMENT querry
            $stmt->execute([':id'=> $this->id]);
            
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            //SET PROPERTIES
            $this->id = $row['id'];
            $this->username = $row['u_user'];
            $this->email = $row['u_email'];
            $this->firstname = $row['firstname'];
            $this->lastname = $row['lastname'];
            $this->bio = $row['bio'];
            $this->picture = $row['picture'];
        }

        //SignUp to movieflix
        public function signup(){
            $sql = "INSERT INTO $this->table(u_user, u_email, u_pwd) VALUES(:username, :email, :password)";

            //prepare
            $stmt = $this->conn->prepare($sql);

            //clean data
            $this->username = htmlspecialchars(strip_tags($this->username));
            $this->email = htmlspecialchars(strip_tags($this->email));
            $this->password = htmlspecialchars(strip_tags($this->password));
            //hash password
            $this->password = password_hash($this->password, PASSWORD_DEFAULT);

            //bind data
            $stmt->bindParam(':username', $this->username);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);


            //EXECUTE
            if($stmt->execute()){
                return true;
            }

            print_f("Error: %s.\n", $stmt->error);
            return false;
        }

        //login to movieflix
        public function login(){
            $sql = "SELECT * FROM $this->table WHERE u_user = :username";

            $stmt = $this->conn->prepare($sql);

            //clean input data
            $this->username = htmlspecialchars(strip_tags($this->username));
            $this->password = htmlspecialchars(strip_tags($this->password));

            $stmt->execute([":username"=> $this->username]);
            $num = $stmt->rowCount();
            if($num < 1){
                return false;
            }else{
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $checkPassword = password_verify($this->password, $row['u_pwd']);

                if($checkPassword === true){
                    return "correct";
                }else{
                    return "incorrect";
                }
            }
            
        }

        public function updateProfile(){
            $sql = "UPDATE $this->table SET firstname=:firstname, lastname=:lastname, bio=:bio, picture=:picture WHERE id=:id";
            //PREPARE STATEMENT
            $stmt = $this->conn->prepare($sql);
            //clean input
            $this->id = htmlspecialchars(strip_tags($this->id));
            $this->firstname = htmlspecialchars(strip_tags($this->firstname));
            $this->lastname = htmlspecialchars(strip_tags($this->lastname));
            $this->bio = htmlspecialchars(strip_tags($this->bio));
            $this->picture = htmlspecialchars(strip_tags($this->picture));

            //Bind params
            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":firstname", $this->firstname);
            $stmt->bindParam(":lastname", $this->lastname);
            $stmt->bindParam(":bio", $this->bio);
            $stmt->bindParam(":picture", $this->picture);

            //execute statement
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }

        }
    }
?>