<?php
    //HEADERS
    header("ACCESS-CONTROL-ORIGIN: *");
    header("CONTENT-TYPE: application/json");

    include_once "../../config/Database.php";
    include_once "../../models/Videos.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $video = new Videos($db);

    //GET ID
    $video->id = isset($_GET['id']) ? $_GET['id'] : die();

    //Call the method to get the values
    $video->getOneVideo();

    //Create Array
    $video_arr = array(
        'id'=> $video->id,
        'userID'=> $video->userID,
        'title'=> $video->title,
        'genre'=>$video->genre,
        'video'=>$video->video,
        'rating'=>$video->rating,
    );

    //Make json file
    print_r(json_encode($video_arr));
?>