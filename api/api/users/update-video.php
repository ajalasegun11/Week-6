<?php
    //HEADERS
    header("Access-Control-Origin: *");
    header("Content-Type: application/json");
    header("Access-Contro-Allow-Methods: POST");
    header("Access-Contro-Aloow-Headers: Access-Contro-Aloow-Headers Content-Type, Access-Contro-Allow-Methods, Authorization, X-Requested-With");

    include_once "../../config/Database.php";
    include_once "../../models/Videos.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE Video MODEL
    $video = new Videos($db);

    $video->id = isset($_POST['id']) ? $_POST['id'] : die();
    $video->title = isset($_POST['title']) ? $_POST['title'] : die();
    $video->genre = isset($_POST['genre']) ? $_POST['genre'] : die();

    //call update video function to insert values to db
    if($video->updateVideo()){
        echo json_encode(
            array("message"=>"Video Updated")
        );
    }else{
        echo json_encode(
            array("message"=>"Video Not Updated")
        );
    }

?>