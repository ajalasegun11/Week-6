<?php
    //HEADERS
    header("Access-Control-Origin: *");
    header("Content-Type: application/json");
    header("Access-Contro-Allow-Methods: POST");
    header("Access-Contro-Aloow-Headers: Access-Contro-Aloow-Headers Content-Type, Access-Contro-Allow-Methods, Authorization, X-Requested-With");

    include_once "../../config/Database.php";
    include_once "../../models/Users.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $user = new Users($db);

    //GET RAW DATA POSTED IN JSON
    $data = json_decode(file_get_contents("php://input"));

    $user->username = $data->username;
    $user->password = $data->password;

    //login to user
    switch($user->login()){
        case "correct":
            echo json_encode(array(
                "message" => "Login successful "
            ));
        break;
        case false:
            echo json_encode(array(
                "message" => "User does not exist, please signup to login"
            ));
        break;
        case "incorrect":
            echo json_encode(array(
                "message" => "Incorrect password"
            ));
        break;
        default:
            echo json_encode(array(
                "message" => "Something went wrong, please retry"
            ));
    }

?>