<?php
    //HEADERS
    header("Access-Control-Origin: *");
    header("Content-Type: application/json");
    header("Access-Contro-Allow-Methods: POST");
    header("Access-Contro-Aloow-Headers: Access-Contro-Aloow-Headers Content-Type, Access-Contro-Allow-Methods, Authorization, X-Requested-With");

    include_once "../../config/Database.php";
    include_once "../../models/Videos.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE Video MODEL
    $video = new Videos($db);

    //Get the file tyoe to ensure it is a video 
    $fileType = $_FILES['video']['type'];

    if($fileType=="video/mp4"||$fileType=="video/ogg"||$fileType=="video/webm"||$fileType=="video/3gp"){
        $filename = $_FILES['video']['name'];
        $fileTmpName = $_FILES['video']['tmp_name'];
        $fileError = $_FILES['video']['error'];
        //set the file path
        $filePath = "../../../uploads2/".$filename;
        $fileDir = "../../../uploads2/";

        //permissions for heroku
        if(!file_exists("$fileDir")){
            mkdir($fileDir, 0777);
        }
        chmod($fileDir, 0777);

        $fh = fopen($filePath,"a+");
        //Move file to directory
        if(!copy($fileTmpName, $filePath)){
            echo json_encode(array(
                "message"=> "File not moved"
            ));
            die();
        }else{
            echo json_encode(array(
                "message"=> "File moved successfully"
            ));
            fclose($fh);
            $video->userID = isset($_POST['userID']) ? $_POST['userID'] : die();
            $video->title = isset($_POST['title']) ? $_POST['title'] : die();
            $video->genre = isset($_POST['genre']) ? $_POST['genre'] : die();
            $video->video = $filename;

            //call the upload video function //if upload successful
            if($video->uploadVideo()){
                echo json_encode(array(
                    "message"=> "Video successfully uploaded"
                ));
                die();
            }else{
                echo json_encode(array(
                    "message"=> "Unable to upload video"
                ));
                die();
            }
        }
    }else{
        echo json_encode(array(
            "message"=> "Please upload a video file"
        ));
        die();
    }

?>