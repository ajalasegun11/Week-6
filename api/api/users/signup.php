<?php
    //HEADERS
    header("Access-Control-Origin: *");
    header("Content-Type: application/json");
    header("Access-Contro-Allow-Methods: POST");
    header("Access-Contro-Aloow-Headers: Access-Contro-Aloow-Headers Content-Type, Access-Contro-Allow-Methods, Authorization, X-Requested-With");

    include_once "../../config/Database.php";
    include_once "../../models/Users.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $user = new Users($db);

    //GET RAW DATA POSTED IN JSON
    $data = json_decode(file_get_contents("php://input"));

    $user->username = $data->username;
    $user->email = $data->email;
    $user->password = $data->password;

    //Create user
    if($user->signup()){
        echo json_encode(array(
            "message"=> "Post created"
        ));
    }else{
        echo json_encode(array(
            "message"=> "Post not created"
        ));
    }

?>