<?php
    //HEADERS
    header("Access-Control-Origin: *");
    header("Content-Type: application/json");
    header("Access-Contro-Allow-Methods: POST");
    header("Access-Contro-Aloow-Headers: Access-Contro-Aloow-Headers Content-Type, Access-Contro-Allow-Methods, Authorization, X-Requested-With");

    include_once "../../config/Database.php";
    include_once "../../models/Users.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $user = new Users($db);

    //Get the file path to store the image 
    if($fileType=="image/jpg"||$fileType=="image/jpeg"||$fileType=="image/png"){
        $filename = $_FILES['file']['name'];
        $fileTmpName = $_FILES['file']['tmp_name'];
        $fileError = $_FILES['file']['error'];
        $filePath = "../../../uploads1/".$filename;
        $fileDir = "../../../uploads1/";

        //permissions for heroku
        if(!file_exists("$fileDir")){
            mkdir($fileDir, 0777);
        }
        chmod($fileDir, 0777);

        //Move file to directory
        $fh = fopen($filePath,"a+");
        if(!copy($fileTmpName, $filePath)){
        
            echo json_encode(array(
                "message"=> "File not moved"
            ));
            die();
        }else{
            
            echo json_encode(array(
                "message"=> "File moved successfully"
            ));
            fclose($fh);
            $user->id = isset($_POST['id']) ? $_POST['id'] : die();
            $user->firstname = isset($_POST['firstname']) ? $_POST['firstname'] : die();
            $user->lastname = isset($_POST['lastname']) ? $_POST['lastname'] : die();
            $user->bio = isset($_POST['bio']) ? $_POST['bio'] : die();
            $user->picture = $filename;
        }
    }else{
        echo json_encode(array(
            "message"=> "Please upload an image file"
        ));
        die();
    }

    //update profile
    if($user->updateProfile()){
        echo json_encode(array(
            "message"=> "Profile Updated"
        ));
    }else{
        echo json_encode(array(
            "message"=> "Profile not Updated"
        ));
    }

?>