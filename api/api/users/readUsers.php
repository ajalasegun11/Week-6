<?php
    //HEADERS
    header("ACCESS-CONTROL-ORIGIN: *");
    header("CONTENT-TYPE: application/json");

    include_once "../../config/Database.php";
    include_once "../../models/Users.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $user = new Users($db);

    //Get users query
    $result = $user->getAllUsers();

    //get row count
    $num = $result->rowCount();

    //check if there is any user
    if($num > 0){
        $users_arr = array();
        $users_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC) ){
            extract($row);
            $user_detail = array(
                'id'=> $id,
                'username'=> $u_user,
                'email'=> $u_email,
                'first name'=>$firstname,
                'last name'=>$lastname,
                'bio'=>$bio,
                'image'=>$picture
            );

            //move each detail to users_arr['data']
            array_push($users_arr['data'], $user_detail);
        }
        echo json_encode($users_arr);
    }else{
        echo json_encode(array(
            'message'=> 'No post found'
        ));
    }

?>