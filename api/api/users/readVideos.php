<?php
    //HEADERS
    header("ACCESS-CONTROL-ORIGIN: *");
    header("CONTENT-TYPE: application/json");

    include_once "../../config/Database.php";
    include_once "../../models/Videos.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $video = new Videos($db);

    //Get users query
    $result = $video->getAllVideos();

    //get row count
    $num = $result->rowCount();

    //check if there is any user
    if($num > 0){
        $video_arr = array();
        $video_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC) ){
            extract($row);
            $user_detail = array(
                'id'=> $id,
                'userID'=> $userID,
                'title'=> $title,
                'genre'=>$genre,
                'video'=>$video,
                'rating'=>$rating,
            );

            //move each detail to video_arr['data']
            array_push($video_arr['data'], $user_detail);
        }
        echo json_encode($video_arr);
    }else{
        echo json_encode(array(
            'message'=> 'No post found'
        ));
    }

?>