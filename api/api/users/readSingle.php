<?php
    //HEADERS
    header("ACCESS-CONTROL-ORIGIN: *");
    header("CONTENT-TYPE: application/json");

    include_once "../../config/Database.php";
    include_once "../../models/Users.php";

    //INSTANTIATE DATABASE
    $database = new Database();
    $db = $database->connect();

    //INSTANTIATE THE USERS MODEL
    $user = new Users($db);

    //GET ID
    $user->id = isset($_GET['id']) ? $_GET['id'] : die();
    #echo $user->id;
    /*if(isset($_GET['id'])){
        echo $_GET['id'];
    }else{
        echo 'No id set';
    }*/

    //Call the method to get the values
    $user->getOneUser();

    //Create Array
    $user_arr = array(
        'id' => $user->id,
        'username'=> $user->username,
        'email'=> $user->email,
        'firstname'=> $user->firstname,
        'lastname'=> $user->lastname,
        'bio'=> $user->bio,
        'image'=> $user->picture,
    );

    //Make json file
    print_r(json_encode($user_arr));
?>