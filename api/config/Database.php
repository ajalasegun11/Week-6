<?php

    class Database{
        //DB PRARAMS
        private $servername = "db4free.net";
        private $username = "db4free_username";
        private $password = "username";
        private $dbname = "db_db4free";
        private $conn;

        //DB CONNECT
        public function connect(){
            $this->conn = null;
            try{
                $this->conn = new PDO("mysql:host=".$this->servername.";dbname=".$this->dbname, $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }catch(PDOException $e){
                echo "Connection error: ".$e->getMessage();
            }
            return $this->conn;
        }
    }
?>