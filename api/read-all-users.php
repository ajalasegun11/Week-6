<?php 
    //headers required
    header("Access-Control-Allow-Origin: * ");
    header("Content-Type: application/json");

    $servername = "db4free.net";
    $username = "db4free_username";
    $password = "username";
    $dbname = "db_db4free";

    $conn = mysqli_connect($servername,$username,$password,$dbname);
    
    $sql = "SELECT * FROM users";
    $result = mysqli_query($conn, $sql);
    
    if(mysqli_num_rows($result) > 0){
        $users_arr = array();
        $users_arr['records'] = array();
        while($row=mysqli_fetch_assoc($result)){
            extract($row);
            $each_user = array(
                'id' => $id,
                'username' => $u_user,
                'email' => $u_email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'bio' => $bio,
                'picture' => $picture 
            );
            array_push($users_arr['records'], $each_user);
        }
        echo json_encode($users_arr);
    }else{
        echo json_encode(array('message'=>"No data found"));
    }

?>