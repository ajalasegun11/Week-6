<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    //Load Composer's autoloader
    require 'vendor/autoload.php';



    $name = $email = $message = "";
    $nameError = $emailErr = $messageErr = "";
    $emailSuccess = "";
    $emailError = "";

    function cleanInput($input){
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    return $input;
    }

$email = "";
$emailErr = "";
$emailSuccess = "";
$emailError = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty($_POST['email'])){
        $emailErr = "Please enter an email address";
    }else{
        $email = cleanInput($_POST['email']);
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $emailErr = "Invalid email address";
        }
    }
    if($emailErr == ""){
        $email = cleanInput($_POST['email']);
        #compile email to a csv file for later use
        $file = fopen('subscribers.csv', 'a');
        $data = array($email);
        fputcsv($file, $data);
        fclose($file);

        #save email address to send confirmation mail
        $file2 = fopen('sendEmailS.csv', 'w');
        $data2 = array($email);
        fputcsv($file2, $data2);
        fclose($file2);

        #get email from file to send confirmatory email
        $file3 = fopen('sendEmailS.csv', 'r');
        $data3 = fgetcsv($file3);					
        $to = $data3[0];
        
        #send email with phpmailer
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers. backup seperated with ;
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'peterwale25@gmail.com';                 // SMTP username
            $mail->Password = 'ajalabistababe';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
        
            //Recipients
            $mail->setFrom('peterwale25@gmail.com', 'Segun Ajala');
            $mail->addAddress($to, '');     // Add a recipient
            //$mail->addAddress('');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');
        
            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'MovieFlix Subscription Successful';
            $mail->Body    = 'Hi,<br><br> Your email '.$to.' has subscribed to our newsletter.<br><br>
            Warm regards,<br>MovieFlix Group';
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
            $mail->send();
            echo 'Subscription successful. Check your email for confirmation.';
        } catch (Exception $e) {
            echo $to.', has subscribed. Check your email for confirmation.  ', $mail->ErrorInfo;
        }
        

        fclose($file3);
    }else{
        $email = "";	
    }
    
}
?>